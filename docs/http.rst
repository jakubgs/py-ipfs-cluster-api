Custom HTTP Client Factory
==========================

`ipfscluster` is based closely on the :mod:`ipfshttpclient` package. However,
due to differing implementation details, the HTTP client provided by that
package wouldn't work with `ipfscluster` API. Specifically, the API provided
by IPFS cluster uses different HTTP methods. The regular IPFS agent API was
simple enough that the method could be auto-detected using the contents of the
request. For the cluster API, this was not the case.

.. automodule:: ipfscluster.http
   :members:
