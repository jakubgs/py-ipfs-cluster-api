Pinning Functionality
=====================

Considering that IPFS is just a set of nodes orchestrating the pinning of files
in IPFS, it is obvious that new pinning endpoints are needed that are distinct
from the IPFS agents running on each node. While the endpoints have similar
interfaces, the cluster endpoints for pinning add the pinned `cid` to the
synchronization functionality of `raft` that maintains consensus among the peers.

.. note:: The `pins` module functions are not called directly; rather
  they are available via the `pins` attribute of the :doc:`client`.

.. autoclass:: ipfscluster.pins.Section
   :members:
