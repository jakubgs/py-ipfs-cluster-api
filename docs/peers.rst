Cluster Peers Functionality
===========================

Every endpoint defined for querying cluster peers through the `/peers` endpoint
is provided by the `peers` module.

.. note:: The `peers` module functions are not called directly; rather
  they are available via the `peers` attribute of the :doc:`client`.

.. autoclass:: ipfscluster.peers.Section
   :members:
