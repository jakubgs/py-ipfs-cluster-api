Cluster Health
==============

IPFS cluster provides API endpoints to monitor the health of the cluster and
peers. These are exposed through the `health` module.

.. note:: The `health` module functions are not called directly; rather
  they are available via the `health` attribute of the :doc:`client`.

.. autoclass:: ipfscluster.health.Section
   :members:
