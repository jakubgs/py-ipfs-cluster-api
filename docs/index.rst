`ipfscluster` API Documentation
===============================

`ipfscluster` provides a high-level interface to make HTTP requests against the
REST API exposed by `ipfs-cluster <https://cluster.ipfs.io/>`_. For installation
and quickstart instructions, see the main repository
`README <https://gitlab.com/clearos/clearfoundation/py-ipfs-cluster-api>`_.

.. toctree::
   :maxdepth: 2

   client.rst
   allocations.rst
   health.rst
   http.rst
   peers.rst
   pins.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
