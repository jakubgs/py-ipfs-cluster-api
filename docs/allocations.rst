Allocation Queries for Cluster
==============================

An IPFS cluster is basically a set of nodes that orchestrate pinning of files
from IPFS. It is useful to know which nodes in the cluster have pinned specific
files from IPFS. The `allocations` module provides this functionality.

.. note:: The `allocations` module functions are not called directly; rather
  they are available via the `allocations` attribute of the :doc:`client`.

.. autoclass:: ipfscluster.allocations.Section
   :members:
