import pytest

def test_ls(client):
    """Tests the `/pins` get methods.
    """
    r = client.pins.ls()
    assert len(r) > 0
    a0 = r[0]
    assert "cid" in a0
    assert "peer_map" in a0

    nid = client.id()["id"]
    cid = a0["cid"]['/']
    r2 = client.pins.ls(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid

    pdata = r2['peer_map'][nid]
    assert pdata["status"] == "pinned"
    assert pdata["error"] == ''

def test_sync(client):
    """Tests the `/pins/sync` post methods.
    """
    r = client.pins.sync()
    assert len(r) == 0

    rl = client.pins.ls()
    a0 = rl[0]
    nid = client.id()["id"]
    cid = a0["cid"]['/']
    r2 = client.pins.sync(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid

    pdata = r2['peer_map'][nid]
    assert pdata["status"] == "pinned"
    assert pdata["error"] == ''

def test_recover(client):
    """Tests the `/pins/recover` post methods.
    """
    r = client.pins.recover()
    assert len(r) > 0
    a0 = r[0]
    nid = client.id()["id"]
    cid = a0["cid"]['/']
    r2 = client.pins.recover(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid

    pdata = r2['peer_map'][nid]
    assert pdata["status"] == "pinned"
    assert pdata["error"] == ''

def test_add(client):
    """Tests adding a pin using a `post` method.
    """
    r = client.pins.ls()
    cid = r[0]["cid"]['/']

    radd = client.pins.add(cid)

    nid = client.id()["id"]
    r2 = client.pins.ls(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid

    pdata = r2['peer_map'][nid]
    assert pdata["status"] == "pinned"
    assert pdata["error"] == ''

def test_rm(client):
    """Tests removing a pin using a `delete` method.
    """
    r = client.pins.ls()
    cid = r[0]["cid"]['/']

    radd = client.pins.rm(cid)
    r2 = client.pins.ls(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid

    nid = client.id()["id"]
    pdata = r2['peer_map'][nid]
    assert pdata["status"] in ["unpinned", "unpinning"]
    assert pdata["error"] == ''
