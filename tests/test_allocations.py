import pytest

def test_ls(client):
    """Tests listing of pin allocations.
    """
    r = client.allocations.ls()
    assert len(r) > 0
    a0 = r[0]
    assert "cid" in a0
    assert "replication_factor_min" in a0

    cid = a0["cid"]['/']
    r2 = client.allocations.ls(cid)
    assert "cid" in r2 and r2["cid"]['/'] == cid
    assert "replication_factor_min" in r2
