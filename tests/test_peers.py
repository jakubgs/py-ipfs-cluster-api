import pytest

def test_peers(client):
    """Tests the `/peers` listing endpoint.
    """
    r = client.peers.ls()
    assert isinstance(r, list)
    assert len(r) > 0
    p1 = r[0]
    assert "id" in p1
    assert len(p1["addresses"]) > 0
    assert p1["error"] == ''

def  test_rm(client):
    """Tests the removal of peers via `delete` method of `/peers/<peerid>`.
    """
    #TODO: we only have one peer in the docker compose at the moment; we
    #can't remove the sole peer in a cluster, so this does nothing for now.
    #Interactive examination of the logs shows that the call is at least
    #working for now.
    from ipfshttpclient.exceptions import StatusError
    nid = client.id()
    with pytest.raises(StatusError):
        client.peers.rm(nid)
