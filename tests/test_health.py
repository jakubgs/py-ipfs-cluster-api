"""Tests the cluster health endpoints.
"""
import pytest

def test_graph(client):
    """Tests the `/health/graph` endpoint.
    """
    r = client.health.graph()
    assert "ClusterID" in r
    assert "ipfs_links" in r and len(r["ipfs_links"]) > 0
    assert len(r["cluster_to_ipfs"]) > 0
