# `ipfscluster` Revision History

## Revision 0.2.0

Added support for `AddParams` and `PinOptions` so that allocations include the
names provided in the initial call with the client. Since this affects the
behavior of previous calls with identical signatures, it has a minor version
increment.

## Revision 0.1.0

Fully-functional HTTP client with ~100% test coverage. Updated license,
authorship and added sphinx documentation for the API. Debugged all the
`docker-in-docker` headaches to get the CI running with our `docker-compose.yml`.

## Revision 0.0.0

Initialization of basic package structure.
